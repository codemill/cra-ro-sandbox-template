#!/bin/bash

set -e

SANDBOX_URL="https://www.rijkshuisstijl.nl/binaries/rijkshuisstijl/documenten/verzamelingen-afbeeldingen/2018/03/06/sandbox-interactieve-infographics/sandbox-versie-2-1-2.zip"
SANDBOX_NAME=sandbox-2.1.2
# DIST=sandbox/sandbox-2.1.2
# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROOT_DIR="$(pwd)"
SANDBOX_DIR=$ROOT_DIR/sandbox

echo $SANDBOX_DIR

# Clean old build artifacts
rm -rf $ROOT_DIR/public/presentation $ROOT_DIR/public/behaviour $ROOT_DIR/public/index.html
rm -rf $SANDBOX_DIR
mkdir $SANDBOX_DIR

# Download and unzip the sandbox
curl -sS -o $SANDBOX_DIR/sandbox.zip $SANDBOX_URL
unzip -qq $SANDBOX_DIR/sandbox.zip -d $SANDBOX_DIR
rm $SANDBOX_DIR/sandbox.zip

# copy presentation and behaviour from sandbox
cp -r $SANDBOX_DIR/$SANDBOX_NAME/presentation $ROOT_DIR/public
cp -r $SANDBOX_DIR/$SANDBOX_NAME/behaviour $ROOT_DIR/public

# create the index.html file
cat $SANDBOX_DIR/$SANDBOX_NAME/mijn-infographic/index.html | head -n 181 > $ROOT_DIR/public/index.html
cat $ROOT_DIR/public/inject.html >> $ROOT_DIR/public/index.html
cat $SANDBOX_DIR/$SANDBOX_NAME/mijn-infographic/index.html | tail -n +197 >> $ROOT_DIR/public/index.html

# Remove sandbox dir
rm -rf $SANDBOX_DIR
