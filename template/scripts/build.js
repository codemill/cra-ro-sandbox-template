const rewire = require('rewire')
const webpack = require('webpack')
const defaults = rewire('react-scripts/scripts/build.js') // If you ejected, use this instead: const defaults = rewire('./build.js')
let config = defaults.__get__('config')

config.plugins.push(
  new webpack.optimize.LimitChunkCountPlugin({
    maxChunks: 1,
  })
)
