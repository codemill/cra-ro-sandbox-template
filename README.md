# @codemill/cra-template-ro-sandbox

This is the TypeScript RO Sandbox template for [Create React App](https://github.com/facebook/create-react-app).

To use this template, add `--template @codemill/ro-sandbox` when creating a new app.

For example:

```sh
npx create-react-app my-app --template @codemill/ro-sandbox

# or

yarn create react-app my-app --template @codemill/ro-sandbox
```
